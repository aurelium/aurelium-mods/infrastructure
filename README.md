# Infrastructure mod for Minetest (Fork for Aurelium Minetest Server)

Maintained by Aurelium Team


## Original mod by :
by Ragnarok AKA Vibender, http://vibender.com/

based on hardly modified:

	Streets mod by webdesigner97, http://forum.minetest.net/viewtopic.php?id=2984/

depends on:

	default
	wool
	moreblocks
	mesecons
	streets
	digilines

## License
Sourcecode: WTFPL, http://sam.zoy.org/wtfpl/
Graphics & Sounds: CC BY-SA 3.0 Unported, http://creativecommons.org/licenses/by-sa/3.0/


